package com.sterlopus.espressonativetest

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.openActionBarOverflowOrOptionsMenu
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class FirstFragmentTests {

    @get:Rule
    val activityRule = ActivityScenarioRule(MainActivity::class.java)


    @Test fun checkNextButtonIsDisplayed() {
        onView(withId(R.id.button_first)).check(matches(isDisplayed()))
    }

    @Test fun checkNextButtonIsEnabled() {
        onView(withId(R.id.button_first)).check(matches(isEnabled()))
    }

    @Test fun checkNextButtonIsClickable() {
        onView(withId(R.id.button_first)).check(matches(isClickable()))
    }

    @Test fun checkIfMovedToSecondFragmentWhenClickNextButton() {
        onView(withId(R.id.button_first)).perform(click())
        onView(withId(R.id.button_second)).check(matches(isDisplayed()))
    }

    @Test fun floatingActionButtonClickResultTest(){
        onView(withId(R.id.fab)).perform(click())
        onView(withId(com.google.android.material.R.id.snackbar_text))
            .check(matches(withText("Replace with your own action")))
    }

    @Test
    fun checkSettingsMenuButton() {
        openActionBarOverflowOrOptionsMenu(InstrumentationRegistry.getInstrumentation().getTargetContext())
        onView(withText("Settings")).check(matches(isDisplayed()))
    }

}